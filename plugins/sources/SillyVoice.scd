// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Original SynthDef : http://sccode.org/1-4Vm by Bruno Ruviaro

~name = \SillyVoice;
~description = "Voicey Synth";
~channelLayout = \mono;

~function = {
	|
	freq = 220,
	amp = 0.5,
	vibratoSpeed = 6,
	vibratoDepth = 4,
	vowel = 0,
	att = 0.01,
	rel = 0.1,
	pos = 0,
	gate = 1
	|

	var in, vibrato, above, below, env, va, ve, vi, vo, vu, snd;

	// calculate vibrato
	// vibratoDepth is number of semitones to go up and down
	above = (freq.cpsmidi + vibratoDepth).midicps - freq;
	below = (freq.cpsmidi - vibratoDepth).midicps - freq;
	vibrato = SinOsc.kr(vibratoSpeed).range(below, above);
	// this is the basic sound source
	in = Saw.ar(Lag.kr(freq) + vibrato);
	// amplitude envelope
	env = Env.asr(att, amp, rel).kr(doneAction: 2, gate: gate);

	va = BBandPass.ar(
		in: in,
		freq: [ 600, 1040, 2250, 2450, 2750 ],
		bw: [ 0.1, 0.0673, 0.0488, 0.0489, 0.0472 ],
		mul: [ 1, 0.4466, 0.3548, 0.3548, 0.1 ]);

	ve = BBandPass.ar(
		in: in,
		freq: [ 400, 1620, 2400, 2800, 3100 ] ,
		bw: [ 0.1, 0.0494, 0.0417, 0.0429, 0.0387 ],
		mul: [ 1, 0.2512, 0.3548, 0.2512, 0.1259 ]);

	vi = BBandPass.ar(
		in: in,
		freq: [ 250, 1750, 2600, 3050, 3340 ] ,
		bw: [ 0.24, 0.0514, 0.0385, 0.0393, 0.0359 ],
		mul: [ 1, 0.0316, 0.1585, 0.0794, 0.0398 ] );

	vo = BBandPass.ar(
		in: in,
		freq:[ 400, 750, 2400, 2600, 2900 ] ,
		bw: [ 0.1, 0.1067, 0.0417, 0.0462, 0.0414 ],
		mul: [ 1, 0.2818, 0.0891, 0.1, 0.01 ]);

	vu = BBandPass.ar(
		in: in,
		freq: [ 350, 600, 2400, 2675, 2950 ],
		bw: [ 0.1143, 0.1333, 0.0417, 0.0449, 0.0407 ],
		mul: [ 1, 0.1, 0.0251, 0.0398, 0.0158 ]);

	snd = SelectX.ar(Lag.kr(vowel, 0.3), [va, ve, vi, vo, vu]);
	snd = Pan2.ar(Mix(snd), pos);
};


~setup = {};