// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// original SynthDef : https://sccode.org/1-4Rh by rwentk
// freq variable is "f"

~name = \SimplePad;
~description = "A Simple Pad";
~channelLayout = \mono;

~function = {
	|
	freq=440,
	width = 0.5,
	modFreq = 1,
	aTime = 5,
	rTime = 5,
	filter = 2,
	filterQ = 0,
	pan = 0.5
	|

	var env = EnvGen.ar(Env([0.01,1,0.01],[aTime, rTime], 'exp'),  doneAction:2);

    var input = Vibrato.ar(VarSaw.ar(freq, 0, LFNoise2.kr(1)), 5, 0.1, 0, 0.2, 0.1, 0.7);
	var theSine = SinOsc.ar(freq);

//	var theSaw = VarSaw.ar(f * 1.5, 0, width);

//  Emulate six unsynched LFOs driving six comparators for a multi-pulse chorus

	var oscs = 6;
	var scaler = 1/oscs;

	var lfo1 = LFTri.ar(modFreq * 1.51);
	var lfo2 = LFTri.ar(modFreq * 1.11);
	var lfo3 = LFTri.ar(modFreq * 1.31);
	var lfo4 = LFTri.ar(modFreq * 0.71);
	var lfo5 = LFTri.ar(modFreq * 0.61);
	var lfo6 = LFTri.ar(modFreq * 0.51);

	var comp1 = input > lfo1;
	var comp2 = input > lfo2;
	var comp3 = input > lfo3;
	var comp4 = input > lfo4;
	var comp5 = input > lfo5;
	var comp6 = input > lfo6;

	var output = scaler * (comp1 + comp2 + comp3 + comp4 + comp5 + comp6);
	//Add a hint of fundamental for body
     output = output + 0.001 * theSine;

	output = 0.01 * LeakDC.ar(output, 0.9995);

	//Doubled Moog with overdrive.
	//Mmmm yeah.
	output = MoogFF.ar(output.tanh * 20.0, (freq * filter)+LFNoise2.ar(1, 400, 1), LFNoise2.ar(0,3.5, 0));
	output = MoogFF.ar(output * 4, freq * LFNoise2.kr(0.2, 6, 4), 0.5);

	output = 2 * env * output.tanh;
};


~setup = {};