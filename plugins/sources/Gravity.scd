// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*       Audio plugin definition

	Each audio plugin should define the following global variables:
	~name: (symbol) name of the spatializer
	~description: (string) a short description
	~function: the actual definition of the plugin
*/

// SynthDef by Victor Comby


~name = \Gravity;
~description = "Random Filtered Impacts";
~channelLayout = \mono;
~function =  { |amp = 0.5, lfospeed = 4, lfospeedmin = 0.1, lfospeedmax = 5|
	var sig, sig2, sig3, sig4, sig5, sigfinal, sigx, rq, ffreq, filtfreqmax, filtfreqmin, sigchord;
	rq = LFNoise1.ar(8).exprange(0.001, 0.8);
	filtfreqmax = LFNoise1.ar(4).exprange(500, 15000);
	filtfreqmin = LFNoise1.ar(4).exprange(1, 100);
	ffreq = LFNoise1.ar(lfospeed).exprange(lfospeedmin,lfospeedmax);
	sig = Saw.ar(Saw.kr(ffreq).exprange(32.70*8, 32.70), Saw.kr(ffreq).exprange(1, 0.5));
	sig2 = Saw.ar(66.41, LFNoise0.kr(lfospeed).exprange(0.02, 0.8));
	sig5 = WhiteNoise.ar(1);
	sig5 = Phaser.ar(sig5,ffreq,1, 0.70, 0.95, 50);
	sigfinal = BLowPass4.ar(
		sig + sig2 + sig5,
		LFTri.kr(ffreq).exprange(20, 18000),
		0.001
	);
	sigfinal = BLowPass4.ar(
		sig + sig2 + sig5,
		Saw.kr(ffreq).exprange(filtfreqmin, filtfreqmax),
		rq
	);
	sigx = sigfinal;
	sigx = Limiter.ar(sigx, 0.4, 0.01);
	sigx = Pan2.ar(Mix(sigx), 0, amp);
};

~setup = {}

