// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*       Audio plugin definition

	Each audio plugin should define the following global variables:
	~name: (symbol) name of the spatializer
	~description: (string) a short description
	~function: the actual definition of the plugin

*/

// based on SATIE's own sndBuffer plugin but adding an envelope

~name = \grainBufASR;

// note:  bufnum must be first arg !!
~description = "Play a buffer with an envelope";
~channelLayout = \mono;


~function = { | bufnum = 0, startPos=0, rate = 1, gate = 1, att = 0.01, rel = 0.1, dur=1, tone=1, loop = 0, trimDB=0, amp=1 |
	var lpassFq, sig, env;

	tone = abs (tone);
	tone = tone.clip(0,1);
	startPos = startPos.clip(0.0, 1.0);

	lpassFq = 100 + (tone**(0.7) * 21900) ;  // used to cut highs when incidence is low
	env = Env.linen(att, dur, rel).kr(gate: gate, doneAction: Done.freeSelf);
	sig = trimDB.dbamp * PlayBuf.ar(1, bufnum, rate: BufRateScale.kr(bufnum) * rate, startPos: BufFrames.kr(bufnum) * startPos, loop: loop);
	BLowPass.ar(sig * env * amp, Lag.kr(lpassFq, 0.02));

};
