// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

SatieConfiguration {

	const <validOrders = #[0, 1, 2, 3];

	// these 7 variables must be defined first
	// so that SatieConfiguration.instVarNames[0..6]
	// returns their names as Symbols inside a Set
	var <server;
	var <listeningFormat = #[];
	var <numAudioAux = 0;
	var <outBusIndex = #[];
	var <ambiOrders = #[];
	var <minOutputBusChannels = 2;
	var <userPluginsDir;

	var <>ambiBus; // array of busses for ambisonics
	var <>debug = false;
	var <satieRoot;
	var <satieUserSupportDir;
	var <>generateSynthdefs = true;


	// Plugins needed by the renderer
	var <>generators;
	var <>effects;
	var <>processes;
	var <>spatializers;
	var <>mappers;
	var <>postprocessors;
	var <>hoa;
	var <>monitoring;

	// other options
	var <>orientationOffsetDeg = #[0, 0];
	var <hoaEncoderType = \wave;

	*new { |server, listeningFormat = #[\stereoListener], numAudioAux = 0, outBusIndex = #[0], ambiOrders = #[], minOutputBusChannels = 2, userPluginsDir = ""|
		server = server ?? { Server.default };

		if(ambiOrders.notEmpty) {
			var valid = ambiOrders.every { |i| validOrders.includes(i) };
			if(valid.not) {
				Error("%: Invalid \'ambiOrders\'. Should be an Array of Integers %".format(thisMethod, validOrders)).throw
			};
		};

		^this.init(server,userPluginsDir).configure(listeningFormat, numAudioAux, outBusIndex, ambiOrders, minOutputBusChannels);
	}

	*init { |server, userPluginsDir|
		server = server ?? { Server.default };


		^super.newCopyArgs(server).init(userPluginsDir);
	}

	*fromJsonFile { |path|
		var json;

		if(path.class !== String) {
			Error("%: argument \'path\' should be a String, was a %".format(thisMethod, path.class)).throw;
		};

		path = path.standardizePath;
		if(File.existsCaseSensitive(path).not) {
			Error("%: file not found %".format(thisMethod, path)).throw;
		};

		json = SatieJson.parseJsonFile(path);
		if(json.isNil) {
			^nil;
		};

		^this.initJson(json);
	}

	*fromJsonString { |string|
		var json;

		if(string.class !== String) {
			Error("%: argument \'string\' should be a String, was a %".format(thisMethod, string.class)).throw;
		};

		json = SatieJson.parseJsonString(string);
		if(json.isNil) {
			^nil;
		};

		^this.initJson(json);
	}

	*initJson { |json|
		var keys = this.instVarNames[0..6];
		var ignoredKeys = [\server, \userPluginsDir];
		var server;

		// remove keys that are allowed to be nil
		ignoredKeys.do({ |k| keys.remove(k)});

		keys.do { |name|
			if(json[name].isNil) {
				Error("%: invalid JSON config. No entry named \'%\' was found".format(thisMethod, name)).throw;
			};
		};

		if(json.server.notNil) {
			switch(json.server.supernova)
				{true} { Server.supernova }
				{ Server.scsynth };
			server = json.server.name !? { |name| Server(name) };
		};

		^this.new(
			server,
			json.listeningFormat,
			json.numAudioAux,
			json.outBusIndex,
			json.ambiOrders,
			json.minOutputBusChannels,
			json.userPluginsDir
		);
	}

	init { |uProvidedPluginParentDir|
		satieRoot = this.class.filenameSymbol.asString.dirname.dirname;

		satieUserSupportDir =  Platform.userAppSupportDir.asString.dirname +/+ "satie"; // set default user plugin path

		if (uProvidedPluginParentDir.size>0,
			{
				userPluginsDir = uProvidedPluginParentDir; // set explicit user plugin parent dir path

				if(debug) {
					"%: initializing with custom user plugins parent-dir path %".format(this.class, userPluginsDir).postln;
				};
		});

		this.initDicts;
		this.initPlugins;
	}

	configure { |listeningFormat = #[\stereoListener], numAudioAux = 0, outBusIndex = #[0], ambiOrders = #[], minOutputBusChannels = 2|
		// take argument values and uses them to set SatieConfiguration's instance variables
		this.setInstVarValues(thisMethod.argNames, listeningFormat, numAudioAux, outBusIndex, ambiOrders, minOutputBusChannels);

		this.validateSpatializerConfig;
		this.setNumOutputChannels;

		if(debug) {
			"%: Satie root directory: %".format(this.class, satieRoot).postln;
			"%: configured using spatializers: %".format(this.class, listeningFormat).postln;
			"%: Satie number of output channels: %".format(this.class, server.options.numOutputBusChannels).postln;
			"%: available plugins:\nSource: %\nEffects: %\nProcesses: %\nSpatializers: %\nMappers: %\nPostProcessors: %".format(
				this.class, generators, effects, processes, spatializers, mappers, postprocessors
			).postln;
		};
	}

	setInstVarValues { |names ...args|
		// first name is 'this' so we skip it
		names[1..].do { |name, index|
			this.instVarPut(name, args[index]);
		}
	}

	initDicts {
		generators = SatiePlugins.new;
		effects = SatiePlugins.new;
		processes = SatiePlugins.new;
		spatializers = SatiePlugins.new;
		mappers = SatiePlugins.new;
		postprocessors = SatiePlugins.new;
		hoa = SatiePlugins.new;
		monitoring = SatiePlugins.new;
	}

	initPlugins {
		var userPlugsPath;

		this.loadPluginDir(satieRoot +/+ "plugins");

		userPlugsPath = satieUserSupportDir +/+ "plugins";
		if (PathName(userPlugsPath).isFolder.not) {
			"There is no user plugins directory at %. Continuing without.".format(userPlugsPath).warn;
		} {
			this.loadPluginDir(userPlugsPath)
		};

		if (userPluginsDir.notNil){
			userPlugsPath = userPluginsDir;
			if (PathName(userPlugsPath).isFolder.not) {
				"There is no user-provided plugins directory at %. Continuing without.".format(userPlugsPath).warn;
			} {
				this.loadPluginDir(userPlugsPath)
			}
		};
	}

	loadPluginDir { |path|
		generators.putAll(SatiePlugins.newAudioPlugin(path +/+ "sources" +/+ "*.scd"));
		effects.putAll(SatiePlugins.newAudioPlugin(path +/+ "effects" +/+ "*.scd"));
		processes.putAll(SatiePlugins.newProcess(path +/+ "processes" +/+ "*.scd"));
		spatializers.putAll(SatiePlugins.newSpatPlugin(path +/+ "spatializers" +/+ "*.scd"));
		mappers.putAll(SatiePlugins.newAudioPlugin(path +/+ "mappers" +/+ "*.scd"));
		postprocessors.putAll(SatiePlugins.newAudioPlugin(path +/+ "postprocessors" +/+ "*.scd"));
		hoa.putAll(SatiePlugins.newAudioPlugin(path +/+ "hoa" +/+ "*.scd"));
		monitoring.putAll(SatiePlugins.newAudioPlugin(path +/+ "monitoring" +/+ "*.scd"));
	}

	validateSpatializerConfig {
		if(listeningFormat.isEmpty) {
			"%: no \'listeningFormat\' provided. SATIE will not be configured to use any spatializers.".format(thisMethod).warn;
			^this
		};

		listeningFormat.do { |format|
			if(spatializers[format].isNil) {
				Error("%: unknown \'listeningFormat\'. No spatializer named % exists.".format(thisMethod, format)).throw;
			};
		};

		if(listeningFormat.size != outBusIndex.size) {
			Error("Mismatched arguments. There should be one \'outBusIndex\' value for each \'listeningFormat\'").throw;
		};
	}

	setNumOutputChannels {
		var numOutputs = 0;

		listeningFormat.do { |format, index|
			var spatializer = this.spatializers[format.asSymbol];
			var channels = outBusIndex[index] + spatializer.numChannels;
			numOutputs = max(numOutputs, channels);
		};

		// keep the larger of minOutputBusChannels and numOutputs
		numOutputs = max(numOutputs, minOutputBusChannels);
		// if numOutputs is greater than the default (2), set the Server's number of outputs
		if(numOutputs > 2) {
			server.options.numOutputBusChannels = numOutputs;
		};
	}

	hoaEncoderType_ { |type|
		if([\wave, \harmonic, \lebedev50].includes(type)) {
			hoaEncoderType = type;
		} {
			Error("%: Invalid argument. Should be one of [\\harmonic, \\wave, \\lebedev50], was %".format(thisMethod, type)).throw;
		}
	}

}
