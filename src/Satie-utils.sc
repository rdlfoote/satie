+ Satie {

	// switchMake provides a convience function for simple projects to facilitate listening format switching between non-ambisonic and ambisonic spatializers.
	// if the ambiOrder argument is non-nil, the correct arugments will be applyed to makeAmbi, otherwise to makeSynthDef.
	// Note that the argument order is the same as makeSynthDef, with one (required) or more additional arguments that can be applied for the makeAmbi case.
	// Limitation: for use only with projects that do not make simultaneous use of Ambisonic and VBAP Listening formats.

	switchMake {
		|
		id,
		srcName,
		preBusArray,
		postBusArray,
		srcPreMonitorFuncsArray,
		spatSymbolArray,
		firstOutputIndexes,
		paramsMapper = \defaultMapper,
		synthArgs = #[]
		ambiOrder,
		ambiEffectPipeline = #[],
		ambiBus
		|

		if(ambiOrder.isNil) {
			// no ambisonics  defined, using standard spatializer(s)

			if(firstOutputIndexes.isNil) {
				var format = config.listeningFormat;
				var series = Array.iota(format.size);
				firstOutputIndexes = spatSymbolArray.collect { |spat|
					var index = series.detect { |i| format[i] === spat };
					if(index.isNil) {
						"%: spatSymbolArray value \'%\' is not a listeningFormat currently in use".format(thisMethod, spat).throw;
					};
					series.remove(index);
					config.spatBus[index];
				};
			};

			this.makeSynthDef(
				id,
				srcName,
				preBusArray,
				postBusArray,
				srcPreMonitorFuncsArray,
				spatSymbolArray,
				firstOutputIndexes,
				paramsMapper,
				synthArgs
			);
		} {
			// else ambisonics defined, using ambisonic spatializer

			// if no ambiBus provided, try and find one based on ambiOrder
			// this is basically what is done in replaceAmbiPostProcessor
			if(ambiBus.isNil && ambiOrder.notNil) {
				var index = config.ambiOrders.detectIndex { |order| order === ambiOrder };
				ambiBus = config.ambiBus[index];
			};

			this.makeAmbi(
				id,
				srcName,
				preBusArray,
				postBusArray,
				srcPreMonitorFuncsArray,
				ambiOrder,
				ambiEffectPipeline,
				ambiBus,
				paramsMapper,
				synthArgs
			);
		}
	}

	getAllPlugins {
		var ret = Dictionary.new();
		ret
		    .add(\generators -> config.generators)
		    .add(\effects -> config.effects)
		    .add(\hoa -> config.hoa)
		    .add(\postprocessors -> config.postprocessors)
		    .add(\monitoring -> config.monitoring);
		^ret;
	}
}
