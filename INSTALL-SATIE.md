# Getting Started with SATIE on Ubuntu and macOS

This document will help get you started with SATIE by guiding you through its installation on _Ubuntu 20.04+_ and _macOS_.
It's recommended that you use SATIE with the latest stable versions of SuperCollider and sc3-plugins. 
For Higher-order Ambisonics, SATIE makes use of the _HOAUGens_ (available in sc3-plugins 3.10+).
SATIE is designed to work with SuperCollider's alternative multithreaded audio server _supernova_, as well as _scsynth_.

In order to use SATIE, you will need:
* SuperCollider 3.11+
* sc3-plugins 3.11+
* SATIE quark
* MathLib and SC-HOA quarks (dependencies pulled by SATIE)

Optionally, you may want to install SATIE in developer mode (cloning the SATIE's repository and installing manually).
In that case, you will also need to install git (https://git-scm.com/download/). 

In-depth documentation and platform-specific installation instructions for SuperCollider and sc3-plugins can be found in their respective GitHub repositories:
* [SuperCollider on GitHub](https://github.com/supercollider/supercollider/tree/main)
* [sc3-plugins on GitHub](https://github.com/supercollider/sc3-plugins/tree/main)

## Before installing SATIE

### macOS

#### Installing SuperCollider

To install SuperCollider, head over to the releases page and download the macOS release binary:

https://github.com/supercollider/supercollider/releases

Extract and drag `SuperCollider` into your `Applications` folder.

#### Installing sc3-plugins

To install the sc3-plugins, head over to the releases page and download the macOS release binary:

https://github.com/supercollider/sc3-plugins/releases

> **Note:** Be sure to download the version of sc3-plugins that most closely matches your version of SuperCollider

Extract and place the sc3-plugins folder in your SuperCollider extensions folder. By default, this folder is located at:

```
~/Library/Application Support/SuperCollider/Extensions
```

### Ubuntu

### Installing SuperCollider and sc3-plugins using pre-built binaries

We maintain Ubuntu packages on our Metalab Package Archive (MPA) for [Ubuntu 22.04.1 LTS (Jammy Jellyfish)](https://gitlab.com/sat-mtl/distribution/mpa-jammy-amd64-nvidia) and [Ubuntu 20.04.5 LTS (Focal Fossa)](https://gitlab.com/sat-mtl/distribution/mpa-focal-amd64-nvidia).

For easy installation on a Debian-based system (tested only on Ubuntu) copy and paste the following block into a terminal if installing the MPA for Ubuntu 22.04.1 LTS (Jammy):

```bash
sudo apt install -y coreutils wget && \
wget -qO - https://sat-mtl.gitlab.io/distribution/mpa-jammy-amd64-nvidia/sat-metalab-mpa-keyring.gpg \
    | gpg --dearmor \
    | sudo dd of=/usr/share/keyrings/sat-metalab-mpa-keyring.gpg && \
echo 'deb [ arch=amd64, signed-by=/usr/share/keyrings/sat-metalab-mpa-keyring.gpg ] https://sat-mtl.gitlab.io/distribution/mpa-jammy-amd64-nvidia/debs/ sat-metalab main main/debug' \
    | sudo tee /etc/apt/sources.list.d/sat-metalab-mpa.list && \
echo 'deb [ arch=amd64, signed-by=/usr/share/keyrings/sat-metalab-mpa-keyring.gpg ] https://sat-mtl.gitlab.io/distribution/mpa-datasets/debs/ sat-metalab main' \
    | sudo tee /etc/apt/sources.list.d/sat-metalab-mpa-datasets.list && \
sudo apt update
```

For Ubuntu 20.04.5 LTS (Focal Fossa), use the instructions available [here](https://gitlab.com/sat-mtl/distribution/mpa-focal-amd64-nvidia).

Then:

`sudo apt install supercollider sc3-plugins`

Then skip to [Install SATIE](#install-satie) for instructions to complete the installation.

### Compiling from source

#### Installing SuperCollider

The following instructions will guide you through the build process on Ubuntu 20.04 and up.
If you are using a prior version of Ubuntu, follow the instructions provided in SuperCollider's [Linux README](https://github.com/supercollider/supercollider/blob/main/README_LINUX.md).

##### Install build dependencies

Before building SuperCollider, you must install its build dependencies. 
The required packages are available to install using Ubuntu's package manager.

Use this command to install the packages:

```bash
sudo apt install build-essential libsndfile1-dev libasound2-dev libjack-jackd2-dev \
libavahi-client-dev libicu-dev libreadline-dev libncurses-dev libfftw3-dev libxt-dev libudev-dev pkg-config git cmake \
qt5-default qt5-qmake qttools5-dev qttools5-dev-tools qtdeclarative5-dev qtpositioning5-dev \
libqt5sensors5-dev libqt5opengl5-dev qtwebengine5-dev libqt5svg5-dev libqt5websockets5-dev
```

##### Clone SuperCollider

You will need to clone the SuperCollider GitHub repository and fetch the project's submodules.

Assuming you want to clone the repository into a folder called `~/src`, run the following commands:

```bash
cd ~/src
git clone --branch main --recurse-submodules https://github.com/supercollider/supercollider.git
cd supercollider
```

##### Build and install

Create a build folder, then configure, make, and install:

```bash
cd ~/src/supercollider
mkdir build && cd build
```

A default Ubuntu will not install the Emacs editor. If you don't know what Emacs is or have no intention of using it with SuperCollider, you will want to disable support for it:

```bash
cmake -DCMAKE_BUILD_TYPE=Release -DNATIVE=ON -DSC_EL=OFF ..   # turn off emacs-based IDE
```

However, if your OS has Emacs installed, you may simply do

```bash
cmake -DCMAKE_BUILD_TYPE=Release -DNATIVE=ON ..
```

```bash
make
sudo make install
sudo ldconfig    # needed when building SuperCollider for the first time
```

#### Installing sc3-plugins

You will need to clone the sc3-plugins GitHub repository and fetch the project's submodules.

Assuming you want to clone the repository into a folder called `~/src`, run the following commands:

```bash
cd ~/src
git clone --branch main --recurse-submodules https://github.com/supercollider/sc3-plugins.git
cd sc3-plugins
```

Create a build folder, then configure, build, and install:

```bash
mkdir build && cd build
cmake -DSC_PATH=../../supercollider/ -DSUPERNOVA=ON -DCMAKE_BUILD_TYPE=Release -DNATIVE=ON ..
make
sudo make install
```

## Raspeberry Pi

### Installing SuperCollider and sc3-plugins using pre-built binaries

We maintain Ubuntu packages on our [Metalab Package Archive (MPA)](https://gitlab.com/sat-mtl/distribution/mpa-bullseye-amd64-rpi).
For easy installation on a Debian-based system (tested only on Ubuntu) copy and paste the following block into a terminal:

```bash
sudo apt install -y coreutils wget && \
wget -qO - https://sat-mtl.gitlab.io/distribution/mpa-bullseye-arm64-rpi/sat-metalab-mpa-keyring.gpg \
    | gpg --dearmor \
    | sudo dd of=/usr/share/keyrings/sat-metalab-mpa-keyring.gpg && \
echo 'deb [ arch=arm64, signed-by=/usr/share/keyrings/sat-metalab-mpa-keyring.gpg ] https://sat-mtl.gitlab.io/distribution/mpa-bullseye-arm64-rpi/debs/ sat-metalab main' \
    | sudo tee /etc/apt/sources.list.d/sat-metalab-mpa.list && \
sudo apt update
```
Then:

```bash
sudo apt install supercollider sc3-plugins
```

Then skip to [Install SATIE](#install-satie) for instructions to complete the installation.

## Windows

### Installing Supercollider

SuperCollider maintainers provide detailed [installation instructions for Windows](https://github.com/supercollider/supercollider/blob/develop/README_WINDOWS.md#installing-supercollider).

### Installing sc3-plugins

In order to install [sc3-plugins](https://github.com/supercollider/sc3-plugins), follow the official [installation guide](https://github.com/supercollider/sc3-plugins#installation)

### Jack Audio Connection Kit

We also recommend installing [JACK](https://jackaudio.org). It allows routing audio signals from arbitrary audio applications to SATIE for spatialization. 
Downloadable installers are [available](https://jackaudio.org/downloads/) for major operating systems.

## Install SATIE

The SATIE quark can be installed directly from within the SuperCollider IDE (scide).

In a new document, write the following line of code and evaluate it by hitting `Ctrl-Enter`:

```supercollider
Quarks.install("SATIE");
```

This will fetch SATIE quark through the official supercollider repository.

If you want to use a local checkout (git-cloned repository) of SATIE code (for development purposes), you can use this command:


```supercollider
Quarks.install("/path/to/checked/out/SATIE");
```

> **Note:** SATIE depends on another quark, called _SC-HOA_, for its Higher-order Ambisonics capabilities. This quark will be installed automatically when installing SATIE through the official supercollider repository.

You must restart the SuperCollider interpreter, or recompile the class library, in order to make SATIE available for use after installation.
