TestSatieJson : SatieUnitTest {

	var jsonPath;

	setUp {
		var dataDir = this.class.filenameSymbol.asString.dirname +/+ "data";
		jsonPath = (
			valid: dataDir +/+ "valid.json",
			invalid: dataDir +/+ "invalid.json",
		);
	}

	test_SatieJson_stringify_booleans {
		[
			[true, "true"],
			[false, "false"]
		].do { |val|
			this.assertEquals(SatieJson.stringify(val[0]), val[1])
		}
	}

	test_SatieJson_stringify_nil {
		this.assertEquals(SatieJson.stringify(nil), "null")
	}

	test_SatieJson_stringify_numbers {
		[
		// test NaN
			[0/0, "null"],
		// test Infinity
			[inf, "null"],
			[-inf, "null"],
		// test Integer
			[2, "2"],
			[-2, "-2"],
			[0xFF, "255"],
			[-0xFF, "-255"],
			[36rZIGZAG, "2147341480"],
			[-36rZIGZAG, "-2147341480"],
			[2r01100, "12"],
			[-2r01100, "-12"],
		// test Float
			[2.0, "2.0"],
			[-2.0, "-2.0"],
			[1e4, "10000.0"],
			[-1e4, "-10000.0"],
			[1e-4, "0.0001"],
			[-1e-4, "-0.0001"],
			[0.5pi, "1.5707963267949"],
			[-0.5pi, "-1.5707963267949"],
			[32r4A.ABC, "138.32360839844"],
			[-32r4A.ABC, "-138.32360839844"],
		].do { |val|
			this.assertEquals(SatieJson.stringify(val[0]), val[1])
		}
	}

	test_SatieJson_stringify_strings {
		this.assertEquals(
			SatieJson.stringify("a string with a tab \t and a newline \n"),
			"a string with a tab %%% and a newline %%%".format($\\, $\\, $t, $\\, $\\, $n).quote
		);
		this.assertEquals(
			SatieJson.stringify("this should stay as is \\n \\t"),
			"this should stay as is %%% %%%".format($\\, $\\, $n, $\\, $\\, $t).quote
		)
	}

	test_SatieJson_stringify_symbols {
		this.assertEquals(
			SatieJson.stringify('\'a Symbol with quotes and spaces\''),
			"'a Symbol with quotes and spaces'".quote
		)
	}

	test_SatieJson_stringify_arrays {
		this.assertEquals(
			SatieJson.stringify(1!5),
			"[ 1, 1, 1, 1, 1 ]"
		)
	}

	test_SatieJson_stringify_dictionaries {
		var string, result;

		string = SatieJson.stringify(Dictionary.newFrom(List[\a, 1, \b, 2, \c, 3]));
		result = [
			"\"a\": 1",
			"\"b\": 2",
			"\"c\": 3"
		].collect { |slice|
			string.contains(slice)
		};

		this.assert(string.beginsWith("{ "), "Stringified Dictionary begins with \"{ \"");
		this.assert(result == [true, true, true], "Stringified Dictionary contains all key-value pairs");
		this.assert(string.endsWith(" }"), "Stringified Dictionary ends with \" }\"");
	}

	test_parseJsonFile_valid_json {
		var keys = SatieConfiguration.instVarNames[0..6];
		var json = SatieJson.parseJsonFile(jsonPath.valid);

		this.assertEquals(
			json.class,
			IdentityDictionary,
			"SatieJson.parseJsonFile should not return nil when JSON file is valid"
		);

		json.keys.do { |key|
			this.assert(
				keys.includes(key),
				"SatieJson.parseJsonFile dictionary should contain key %".format(key)
			);
		};
	}

	test_parseJsonFile_invalid_json {
		var json = jsonPath.invalid;

		json = SatieJson.parseJsonFile(json);
		this.assertEquals(
			json,
			nil,
			"SatieJson.parseJsonFile should return nil when JSON could not be parsed"
		);
	}

	test_parseJsonString_valid_json {
		var keys = SatieConfiguration.instVarNames[0..6];
		var json = File.readAllString(jsonPath.valid);

		json = SatieJson.parseJsonString(json);
		this.assertEquals(
			json.class,
			IdentityDictionary,
			"SatieJson.parseJsonString should not return nil when JSON string is valid"
		);

		json.keys.do { |key|
			this.assert(
				keys.includes(key),
				"SatieJson.parseJsonString dictionary should contain key %".format(key)
			);
		};
	}

	test_parseJsonString_invalid_json {
		var json = File.readAllString(jsonPath.invalid);

		json = SatieJson.parseJsonString(json);
		this.assertEquals(
			json,
			nil,
			"SatieJson.parseJsonString should return nil when JSON could not be parsed"
		);
	}

	test_parseJsonConfig_exception {
		var dict = Dictionary.newFrom(List[\foo, \bar]);

		this.assertException(
			{ SatieJson.parseJsonConfig(dict) },
			Error,
			"SatieJson.parseJsonConfig should throw an error when no entry named 'satie_configuration' was found"
		);
	}

	test_convertJsonDictionary {
		var parsed = jsonPath.valid.parseJSONFile;
		var json;

		this.assertNoException(
			{ json = SatieJson.convertJsonDictionary(parsed); },
			"SatieJson.convertJsonDictionary should execute without throwing an Error"
		);

		this.assertEquals(
			json.class,
			IdentityDictionary,
			"SatieJson.convertJsonDictionary should convert parsed Dictionary into an IdentityDictionary"
		);

		this.assertEquals(
			json.know,
			true,
			"SatieJson.convertJsonDictionary should return a known IdentityDictionary"
		);
	}

	test_convertJsonString {
		var str;

		str = "true";
		this.assertEquals(
			SatieJson.convertJsonString(str),
			true,
			"Return true when string was \"true\""
		);

		str = "false";
		this.assertEquals(
			SatieJson.convertJsonString(str),
			false,
			"Return false when string was \"false\""
		);

		str = "-12.34";
		this.assertEquals(
			SatieJson.convertJsonString(str),
			-12.34,
			"Call SatieJson.tryNumberConversion when string is neither \"true\" or \"false\""
		);

		str = "foobar";
		this.assertEquals(
			SatieJson.convertJsonString(str),
			\foobar,
			"Fallback to converting input string into a Symbol when all other options fail"
		);
	}

	test_handleValueType {
		var value;

		value = SatieJson.handleValueType(nil);
		this.assertEquals(
			value.class,
			Nil,
			"Return a nil when value's class is Nil"
		);

		value = SatieJson.handleValueType(["1", "2", "3"]);
		this.assertEquals(
			value.class,
			Array,
			"Return an array when value's class is Array"
		);

		value = SatieJson.handleValueType(Dictionary.newFrom(List["a", "1", "b", "2"]));
		this.assertEquals(
			value.class,
			IdentityDictionary,
			"Return an IdentityDictionary when value's class is Dictionary"
		);

		value = SatieJson.handleValueType("foobar");
		this.assert(
			value.class !== String,
			"Return something other than a String when value's class was a String"
		);
	}

	test_convertJsonArray {
		var array = ["1", ["foobar"], "true", "false", nil];
		var result;

		result = SatieJson.convertJsonArray(array);
		this.assertEquals(
			result,
			[1, [\foobar], true, false, nil],
			"SatieJson.convertJsonArray should convert all inner values into appropriate types"
		);

		// test an Array containing a Dictionary
		result = SatieJson.convertJsonArray([Dictionary.newFrom(List["a", "1"])]);
		this.assertEquals(
			result[0].class,
			IdentityDictionary,
			"Convert Dictionary to an IdentityDictionary when when found inside an array"
		);
		result.do { |dict|
			dict.keysValuesDo { |key, value|
				this.assertEquals(
					key,
					\a,
					"Key should be the Symbol \\a"
				);
				this.assertEquals(
					value,
					1,
					"Value should be an Integer 1"
				);
			};
		};
	}

	test_tryNumberConversion {
		var str;

		str = "123a";
		this.assertEquals(
			SatieJson.tryNumberConversion(str),
			nil,
			"Return nil when last char is not a number from 0-9"
		);

		str = "12a4";
		this.assertEquals(
			SatieJson.tryNumberConversion(str),
			nil,
			"Return nil when string can't be converted to a number"
		);

		str = "-12.34";
		this.assertEquals(
			SatieJson.tryNumberConversion(str),
			-12.34,
			"Return a number when string contains a nagetive float"
		);

		str = "-1234";
		this.assertEquals(
			SatieJson.tryNumberConversion(str),
			-1234,
			"Return a number when string contains a nagetive integer"
		);

		str = "12.34";
		this.assertEquals(
			SatieJson.tryNumberConversion(str),
			12.34,
			"Return a number when string contains a float"
		);

		str = "1234";
		this.assertEquals(
			SatieJson.tryNumberConversion(str),
			1234,
			"Return a number when string contains a integer"
		);
	}

}
