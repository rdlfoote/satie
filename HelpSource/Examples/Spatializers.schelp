TITLE:: SATIE Spatializers
SUMMARY:: Overview of spatialization plugins in SATIE
CATEGORIES:: Libraries>SATIE, Libraries>HOA, UGens > Multichannel > Panners
RELATED:: Overview/SATIE-Overview, Classes/SpatializerPlugin, Examples/Ambi

Go back to link::Overview/SATIE-Overview::

Previous Section: link::Examples/SATIE-Basics::

Next Section: link::Examples/Processes::


SECTION:: Overview

SATIE provides two types of spatializers: amplitude panning-based spatializers (Stereophonic, Quadrophonic, 2D/3D VBAP) and Higher-order Ambisonics (HOA) up to the 3rd order.

SUBSECTION:: Example amplitude panning-based spatializers

TABLE::
##  STRONG::Name::|| STRONG::Number of channels::||STRONG::Description::
##  cube || 8 || 8 channel 3D layout
##  domeVBAP || 31 || 31 channel speaker dome (SAT)
##  iaiaVBAP || 24 || 24 channel speaker dome (IAIA)
##  quadpanaz || 4 || 4 channel 2D layout
##  stereoListener || 2 || Stereo panner
##  VBAP1474 || 16 || 16 channel speaker dome (IEM)
::

SUBSECTION:: Example HOA plugins

HOA plugins make use the SC-HOA quark by Florian Grond.

SUBSECTION:: Example HOA spatializers
TABLE::
## STRONG::Name:: || STRONG::Description::
##  HOABinaural || Binaural decoder based on the HOABinaural UGen
::

SUBSECTION:: Example HOA processing plugins

Besides ambisonic spatializers, HOA plugins also include processors specific for b-format signals.

TABLE::
## STRONG::Name:: || STRONG::Description::
##  BeamCardio || Filters along a specifed direction (hyper cardioid pattern)
##  BeamDirac || Filters along a specifed direction (truncated Dirac)
##  Mirror || Mirror soundfield
##  Rotate || Rotate the sound field in 3 axes
##  RotateAz || Rotation around the z axis
::

SUBSECTION:: Subwoofers

Subwoofer channels can be added by way of using a single-channel "spatializer" called TELETYPE::monoSpat:: and adding a post-processor called TELETYPE::sub::

So we can create a 3 channel setup for 2.1:
CODE::
(
~satieConfiguration = SatieConfiguration.new(s, [\stereoListener, \monoSpat], outBusIndex: [0, 2], minOutputBusChannels: 3);
~satie = Satie.new(~satieConfiguration);
~satie.boot();
s.meter;
)
::

Instantiate a sound source and a postprocessor attached to the second spatializer and indexed on the output channel 3:
CODE::
~satie.makeSourceInstance(\noizy, \default, \default);
~satie.replacePostProcessor([\sub], 2, 1);
::
Play the source
CODE::
~satie.groupInstances[\default][\noizy].set(\gainDB, -30, \aziDeg, 0, \eleDeg, 10)
::
Add subwoofer channel:
CODE::
~satie.postProcessors[\post_proc_1].set(\sub_gainDB, -30, \sub_lpHz, 60);
::

Go back to link::Overview/SATIE-Overview::

Previous Section: link::Examples/SATIE-Basics::

Next Section: link::Examples/Processes::
